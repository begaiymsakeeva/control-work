<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\NewsType;
use App\Repository\CommentRepository;
use App\Repository\NewsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfileController extends Controller
{
    /**
     * @Route("/myProfile")
     * @param NewsRepository $newsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(NewsRepository $newsRepository)
    {
        $allNews = $newsRepository->getAllActive();

        return $this->render('my_profile/index.html.twig', [
            'controller_name' => 'MyProfileController',
            'allNews' => $allNews
        ]);
    }

    /** @Route("/addNews")
     * @Method({"POST"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addNewsAction(\Symfony\Component\HttpFoundation\Request $request){


        /** @var User $user  */
        $user =  $this->getUser();

        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $news->setAuthor($user);
            $news->setCreatedAt(new \DateTime());
            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('app_profile_index');
        }

            return $this->render('my_profile/add_news.html.twig', array(
            'form'=>$form->createView(),
        ));}

    /**
     * @Route("/showNews/{id}")
     *
     * @param News $news
     * @param CommentRepository $commentRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showNewsAction(
        News $news,
        CommentRepository $commentRepository
    )
    {

        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment, [
            'action' => $this->generateUrl('app_profile_addcomment', ['id' => $news->getId()])
        ]);

        $comments = $commentRepository->getAllByNews($news);

        return $this->render('my_profile/showNews.html.twig', [
            'news' => $news,
            'comments' => $comments,
            'commentForm' => $commentForm->createView(),

        ]);
    }

    /**
     * @Route("/addComment/{id}")
     *
     * @param Request $request
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addCommentAction(
        Request $request,
        News $news,
        ObjectManager $manager
    )
    {
        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment
                ->setUser($this->getUser())
                ->setNews($news);
            $manager->persist($comment);
            $manager->flush();
        }

        return $this->redirectToRoute('app_profile_shownews', ['id' => $news->getId()]);

}}
