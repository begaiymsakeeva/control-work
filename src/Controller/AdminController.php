<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\News;
use App\Form\AdminsNewsType;
use App\Repository\NewsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    /**
     * @Route("/admin-index")
     * @param NewsRepository $newsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(NewsRepository $newsRepository)
    {
        $allNews = $newsRepository->getAll();

        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'allNews' => $allNews
        ]);
    }
    /**
     * @Route("/admin-addNews")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addNewsAction(Request $request, ObjectManager $manager)
    {
        $news = new News();

        $form = $this->createForm(AdminsNewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setAuthor($this->getUser());
            if ($news->getActive()) {
                if (!$news->getPublishedAt()) {
                    $news->setPublishedAt(new \DateTime());
                }
            } else {
                $news->setPublishedAt(null);
            }
            $manager->persist($news);
            $manager->flush();

            return $this->redirectToRoute('app_admin_index');
        }

        return $this->render('admin/add_news.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin-publish/{id}")
     *
     * @param News $news
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function publishAction(News $news, ObjectManager $manager)
    {
        if (!$news->getActive()) {
            $news->setActive(true)
            ->setPublishedAt(new \DateTime());
            $manager->persist($news);
            $manager->flush();
        }

        return $this->redirectToRoute('app_admin_index');
    }

    /**
     * @Route("/delete-comment/{id}")
     *
     * @param Comment $comment
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteCommentAction(Comment $comment, ObjectManager $manager)
    {
        $manager->remove($comment);
        $manager->flush();

        return $this->redirectToRoute('app_admin_index');
    }
}
