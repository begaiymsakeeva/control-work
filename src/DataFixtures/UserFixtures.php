<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    const USER_ONE = 'user1@mail.ru';
    const USER_TWO = 'user2@mail.ru';
    const USER_THREE = 'user3@mail.ru';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $superAdmin = new User();
        $superAdmin
            ->setUsername("admin")
            ->setEnabled(true)
            ->setEmail("admin@mail.ru")
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_ADMIN']);
        ;
        $manager->persist($superAdmin);

        $user1 = new User();
        $user1
            ->setUsername(self::USER_ONE)
            ->setEnabled(true)
            ->setEmail(self::USER_ONE)
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_USER']);
        ;
        $manager->persist($user1);

        $user2 = new User();
        $user2
            ->setUsername(self::USER_TWO)
            ->setEnabled(true)
            ->setEmail(self::USER_TWO)
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_USER']);
        ;
        $manager->persist($user2);

        $user3 = new User();
        $user3
            ->setUsername(self::USER_THREE)
            ->setEnabled(true)
            ->setEmail(self::USER_THREE)
            ->setPlainPassword('12345')
            ->setRoles(['ROLE_USER']);
        ;
        $manager->persist($user3);

        $manager->flush();

        $this->addReference(self::USER_ONE, $user1);
        $this->addReference(self::USER_TWO, $user2);
        $this->addReference(self::USER_THREE, $user3);
    }
}