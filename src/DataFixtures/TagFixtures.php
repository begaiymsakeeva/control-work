<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $title){
            $tag = new Tag();
            $tag->setTitle($title);
            $manager->persist($tag);

            $this->addReference($title, $tag);
        }

        $manager->flush();
    }

    private function getData()
    {
        return ['звезды', 'отношения', 'в мире кино'];
    }
}