<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $news = new News();
        $news
            ->setTitle('Ольга Куриленко вышла на подиум на Неделе моды в Лондоне')
            ->setImage('pic2.jpg')
            ->setDescription('В Лондоне продолжается Неделя моды. На показе коллекции британской марки Temperley на подиум вышла актриса Ольга Куриленко.

Этот показ для марки стал своего рода прощальным: третий показ завершил цикл и теперь презентации коллекций будут совершенно другими. Впрочем, креативный директор бренда Алиса Темперли не раскрыла подробностей.')
            ->addCategory($this->getReference('стиль жизни'))
            ->addTag($this->getReference('звезды'))
            ->setAuthor($this->getReference(UserFixtures::USER_ONE))
            ->setActive('true')
            ->setPublishedAt(new \DateTime());
        $manager->persist($news);

        $news = new News();
        $news
            ->setTitle('Ким Кардашьян пришла на вечеринку в костюме из долларов')
            ->setImage('pic3.jpg')
            ->setDescription('37-летняя Ким Кардашьян (Kim Kardashian) продолжает радовать поклонников стильными выходами в свет. Ким не боится показаться смешной: она часто первая «выгуливает» самые смелые тренды, начиная от латекса и заканчивая неоном. Ее муж, рэпер Канье Уэст (Kanye West) не отстает и часто тоже из первых примеряет самые затейливые тренды.

На этот раз жена Ким отправилась на вечеринку для миллионеров в Лос-Анджелесе. Для такого случая она выбрала наряд, полностью покрытый долларовыми принтами!')
            ->addCategory($this->getReference('стиль жизни'))
            ->addTag($this->getReference('звезды'))
            ->setAuthor($this->getReference(UserFixtures::USER_TWO));
        $manager->persist($news);

        $news = new News();
        $news
            ->setTitle('18 видов поцелуев и их значение')
            ->setImage('pic4.jpg')
            ->setDescription('Приготовьтесь, будет интересно! Познавательный YouTube-канал «Профессор Гуглов» подготовил видео про поцелуи. На размышления о значении тех или иных видов поцелуев (а оказывается, они делятся на категории) YouTube-блогера натолкнула книга Шерил Киршенбаум «Наука поцелуя».

Согласно теории Шерил, существует более ста видов поцелуев, выражающих абсолютно разные чувства. Из них были отобраны 18 самых распространенных, и ознакомиться с информацией в видеорежиме можно, посмотрев ролик (а мы далее приведем краткую выжимку из него):')
            ->addCategory($this->getReference('психология'))
            ->addTag($this->getReference('отношения'))
            ->setAuthor($this->getReference(UserFixtures::USER_THREE))
            ->setActive('true')
            ->setPublishedAt(new \DateTime());

        $manager->persist($news);

        $news = new News();
        $news
            ->setTitle('Почему мы расстаемся с людьми, которые нам нравятся?')
            ->setImage('pic1.jpg')
            ->setDescription('На образовательном канале In Cor Cadit вышло новое видео, посвященное отношениям.

За три с половиной минуты в анимационном ролике рассказывается, почему нам так часто бывает непривычно, когда нас любят. Главная причина, по мнению авторов ролика, скрывается в нашей тайной нелюбви к себе. Пока мы не полюбим себя, мы не сможем признать, что нас может полюбить кто-то другой.

Данный ролик был создан командой The School of Life — одной из самых популярных создателей подобного научно-просветительского контента в YouTube. Общее количество просмотров данного канала превышает 250 миллионов. К счастью, ролики доступны в переводе.')
            ->addCategory($this->getReference('психология'))
            ->addTag($this->getReference('отношения'))
            ->setAuthor($this->getReference(UserFixtures::USER_ONE));
        $manager->persist($news);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class
        ];
    }
}
