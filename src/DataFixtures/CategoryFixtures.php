<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $title){
            $category = new Category();
            $category->setTitle($title);
            $manager->persist($category);

            $this->addReference($title, $category);
        }

        $manager->flush();
    }

    private function getData()
    {
        return ['диета', 'макияж', 'психология', 'стиль жизни', 'политика'];
    }
}
